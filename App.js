/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import Video from "react-native-video";
// import LightVideo from "./1.mp4";
import RNFetchBlob from "react-native-fetch-blob";





const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
class App extends Component<Props> {

  constructor(props) {
    super(props)

    this.state = {
      videoPath: ""
    };

  }

  componentWillMount() {
    // let filename = "video"
    let videoName = Math.random() .toString(36) .substring(7);
    console.log("Downloading Video");
    let dirs = RNFetchBlob.fs.dirs;
    filePath = `${dirs.DownloadDir}/${videoName}.${"mp4"}`;
    // var newpath = RNFetchBlob.fs.dirs.DownloadDir + "/video_" + videoName + ".mp4";

    RNFetchBlob.config({
      // add this option that makes response data to be stored as a file,
      // this is much more performant.
      // fileCache: true,
      // appendExt: "mp4"
      path: filePath,
      title: `${videoName}.mp4`,
      description: "Video Downloaded",
      mediaScannable: true,
      mime: "video/mp4",
      notification: true
    //   addAndroidDownloads: {
    // }
    })
      .fetch("GET", "http://vjsoft.org/video/1.mp4", {
        "Cache-Control": "no-store"
      })
      .then(res => {
        // console.log("Saving File");
        // console.log("The file saved to ", res.path());
        // console.log(res);
        // console.log("After Saving File");
        var videoPath =
          Platform.OS === "android"
            ? "file://" + res.path()
            : "" + res.path();

        this.setState({ videoPath });
      });
  }

  render() {
    var videoPlayer;
    if(this.state.videoPath) {
      videoPlayer = (
        <Video
            source={{ uri: this.state.videoPath }}
            resizeMode="fit"
            style={StyleSheet.absoluteFill}
          />
          )
    }


    return (
      <View style={styles.container}>
        <Text>Video Will Play Shortly</Text>
        {videoPlayer}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});


export default App;